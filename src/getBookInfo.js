const request = require('superagent')
const config = require('../config.js')

// this function receives a single book ID and then promises a description
const getDescr = (bookID) => {
  return new Promise((resolve, reject) => {
    request
        .get(config.grs.bookInfoURL + bookID)
        .end((err, res) => {
          if (err) {
            return reject(err)
          }
          // result for the bookids being parsed into a json
          const jsonres = JSON.parse(res.text)
          if (!jsonres.data || !jsonres.data.length) {
            return resolve('Sorry but there is no description available for this book.')
          }
          return resolve(jsonres.data[0].summary)
        })
  })
}
// receives the books object, matches it against the given book and then gets its description
const BookInfo = (memory) => {
  return new Promise((resolve, reject) => {
    if (!memory.book || !memory.book.raw) {
      return reject()
    }
    memory.book.raw = (memory.book.raw).replace(/\ /g, '_')
    getDescr(memory.book.raw)
      .then(bookDescription => {
        return resolve(bookDescription)
      })
      .catch(err => {
        return reject(err)
      })
  })
}
module.exports = BookInfo
