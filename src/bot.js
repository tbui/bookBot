// MODULES IMPORT
import * as recast from 'recastai'
import slack from '@slack/client'
const BookInfo = require('./getBookInfo.js')
const findBooks = require('./findBook.js')
const formatBooksList = require('./formatBooksList.js')
import config from '../config'

// RECAST.AI INIT: Language is optionnal
const recastClient = new recast.Client(config.recast.token, config.recast.language)

// SLACK CLIENT INIT
const SlackClient = slack.RtmClient
const slackEvent = slack.RTM_EVENTS
const rtm = new SlackClient(config.slack.token, { logLevel: 'false' })
rtm.start()

// EVENT: Message received on Slack
rtm.on(slackEvent.MESSAGE, (message) => {
  const user = rtm.dataStore.getUserById(message.user)
  const dm = rtm.dataStore.getDMByName(user.name).id

  // CALL TO RECAST.AI: message.user contains a unique ID of your conversation in Slack
  // The conversationToken is what lets Recast.AI identify your conversation.
  // As message.user is what identifies your Slack conversation, you can use it as conversationToken.
  recastClient.textConverse(message.text, { conversationToken: message.user })
  .then((res) => {
    const replies = res.replies
    const action = res.action

    if (!replies.length) {
      rtm.sendMessage('I didn\'t understand... Sorry :(', dm)
      return
    }
    if (action && action.done) {
      handleAction(action, res, user, dm)
    }
    replies.forEach(reply => rtm.sendMessage(reply, dm))
  })
  .catch(err => rtm.sendMessage('I\'m getting tired, I hope I was able to help you', dm))
})

const handleAction = (action, res, user, dm) => {
  // so if the user agrees that it's indeed the right writer
  if (action.slug === 'agree-1' && action.done) {
    // putting the memory in here to extract the writer's name
    findBooks(res.memory)
        .then(booksObject => {
          // takes the object with the book titles and makes it sendable
          const booksList = formatBooksList(booksObject)
          rtm.sendMessage(booksList, dm)
            .catch(err => rtm.sendMessage('Sorry but there is nothing available for this writer.', dm))
        })
        .catch(err => rtm.sendMessage('Sorry but there is nothing available for this writer.', dm))
  } else if (action.slug === 'whichbook') {
    // here we fire back the info of whatever book was chosen
    BookInfo(res.memory)
        .then(bookinfos => {
          rtm.sendMessage(bookinfos, dm)
          .catch(err => rtm.sendMessage('Sorry but there is no description available for this book.', dm))
        })
        .catch(err => rtm.sendMessage('Sorry but something went wrong.', dm))
  }
}
