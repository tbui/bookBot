const request = require('superagent')
const config = require('../config.js')

// this function shall return a promise for an object of strings of book names
const findBooks = (memory) => {
  // this makes the writer's name in the memory API ready
  return new Promise((resolve, reject) => {
    if (!memory.writer || !memory.writer.value) {
      return reject()
    }
    memory.writer.value = memory.writer.value.replace(/\ /g, '_')
    request
      .get(config.grs.authorInfoURL + memory.writer.value)
      .end((err, res) => {
        if (err) {
          return reject()
        }

        // result for the bookids being parsed into a json
        const jsonres = JSON.parse(res.text)
        const books = []

        if (jsonres.data && jsonres.data.length && jsonres.data[0].book_ids) {
          for (let i = 0; jsonres.data[0].book_ids[i]; i++) {
            books[i] = (jsonres.data[0].book_ids[i].replace(/\_/g, ' '))
          }
        }

        return resolve(books)
      })
  })
}
module.exports = findBooks
